package com.leroy.ronan.cache.memory;

import java.time.Duration;
import java.util.function.Function;

import com.leroy.ronan.cache.Cache;
import com.leroy.ronan.cache.CacheBuilder;

public class InMemoryCacheBuilder<K, T> extends CacheBuilder<K, T>{

	@Override
	protected Cache<K, T> build(Function<K, T> provider, Duration delay, Duration timeout) {
		return new InMemoryCache<>(provider, delay, timeout);
	}
}
