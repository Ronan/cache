package com.leroy.ronan.cache.memory;

import java.time.Instant;

import com.leroy.ronan.cache.CacheEntry;

public class InMemoryCacheEntry<T> implements CacheEntry<T> {

	private T data;
	private Instant creation;
	
	public InMemoryCacheEntry() {
		super();
	}
	
	@Override
	public boolean isEmpty() {
		return data == null;
	}

	@Override
	public void store(T data) {
		this.creation = Instant.now();
		this.data = data;
	}

	@Override
	public T get() {
		return data;
	}

	@Override
	public Instant getCreation() {
		return this.creation;
	}

}
