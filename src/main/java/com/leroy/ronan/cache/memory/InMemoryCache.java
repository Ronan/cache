package com.leroy.ronan.cache.memory;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import com.leroy.ronan.cache.Cache;
import com.leroy.ronan.cache.CacheEntry;

public class InMemoryCache<K, T> extends Cache<K, T> {

	private Map<K, CacheEntry<T>> memory;

	protected InMemoryCache(Function<K, T> provider, Duration delay, Duration timeout) {
		super(provider, delay, timeout);
		this.memory = new HashMap<>();
	}
	
	protected InMemoryCache(Function<K, T> provider, Duration delay) {
		this(provider, delay, null);
	}

	protected InMemoryCache(Function<K, T> provider) {
		this(provider, null);
	}

	@Override
	protected CacheEntry<T> retreive(K key) {
		CacheEntry<T> res = memory.get(key);
		if (res == null) {
			res = new InMemoryCacheEntry<>();
			memory.put(key, res);
		}
		return res;
	}

}
