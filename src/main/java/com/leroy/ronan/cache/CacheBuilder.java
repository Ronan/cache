package com.leroy.ronan.cache;

import java.time.Duration;
import java.util.function.Function;

public abstract class CacheBuilder<K, T> {

	private Function<K, T> provider;
	private Duration delay;
	private Duration timeout;
	
	public CacheBuilder<K, T> from(Function<K, T> provider) {
		this.provider = provider;
		return this;
	}

	public CacheBuilder<K, T> expiringAfter(Duration delay) {
		this.delay = delay;
		return this;
	}

	public CacheBuilder<K, T> timingOutAfter(Duration timeout) {
		this.timeout = timeout;
		return this;
	}

	public Cache<K, T> build(){
		return build(provider, delay, timeout);
	}
	
	protected abstract Cache<K, T> build(Function<K, T> provider, Duration delay, Duration timeout);

}
