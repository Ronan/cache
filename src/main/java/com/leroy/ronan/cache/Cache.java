package com.leroy.ronan.cache;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

public abstract class Cache<K, T> {

    private Function<K, T> provider;
    private Duration expirationDelay;
    private Duration timeout;

    protected Cache(Function<K, T> provider) {
        this.provider = provider;
    }
    
    protected Cache(Function<K, T> provider, Duration delay, Duration timeout) {
        this(provider);
        this.expirationDelay = delay;
        this.timeout = timeout;
    }

    protected abstract CacheEntry<T> retreive(K key);

    public T get(K key) {
        CacheEntry<T> entry = this.retreive(key);
        if (   entry.isEmpty()
            || expirationDelay != null && isExpired(entry)) {
            if (this.timeout != null) {
                CompletableFuture<T> future = CompletableFuture.supplyAsync(() -> retreiveAndStore(key, entry));
                try {
                    future.get(timeout.toMillis(), TimeUnit.MILLISECONDS);
                } catch (InterruptedException | ExecutionException | TimeoutException e) {
                }
            } else {
                retreiveAndStore(key, entry);
            }
        }
        return entry.get();
    }

    private T retreiveAndStore(K key, CacheEntry<T> entry) {
        T data = provider.apply(key);
        if (data != null) {
            entry.store(data);
        }
        return data;
    }

    private boolean isExpired(CacheEntry<T> entry) {
        return Duration.between(
                entry.getCreation(),
                Instant.now()
                ).compareTo(expirationDelay) > 0;
    }
}
