package com.leroy.ronan.cache;

import java.time.Instant;

public interface CacheEntry<T> {

	boolean isEmpty();
	void store(T data);
	T get();
	Instant getCreation();

}
