package com.leroy.ronan.cache.persisted;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Instant;
import java.util.function.Function;

import org.apache.log4j.Logger;

import com.leroy.ronan.cache.CacheEntry;

public class FilePersistedCacheEntry<T> implements CacheEntry<T> {

    private static final Logger log = Logger.getLogger(new Object() { }.getClass().getEnclosingClass());
	
	private File file;
	
	private Function<T, byte[]> toBytes;
	private Function<byte[], T> fromBytes;
	
	public FilePersistedCacheEntry(File file, Function<T, byte[]> toBytes, Function<byte[], T> fromBytes) {
		super();
		this.file = file;
		this.toBytes = toBytes;
		this.fromBytes = fromBytes;
	}

	@Override
	public boolean isEmpty() {
		return !file.exists();
	}

	@Override
	public void store(T data) {
		try {
			file.delete();
			file.createNewFile();
			Files.write(file.toPath(), toBytes.apply(data));
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public T get() {
		T res = null;
		try {
			res = fromBytes.apply(Files.readAllBytes(file.toPath()));
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return res;
	}

	@Override
	public Instant getCreation() {
		Instant res = Instant.now();
		try {
			return Files.getLastModifiedTime(this.file.toPath()).toInstant();
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return res;
	}
}
