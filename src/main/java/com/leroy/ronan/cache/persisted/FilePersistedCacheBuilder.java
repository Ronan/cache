package com.leroy.ronan.cache.persisted;

import java.io.File;
import java.time.Duration;
import java.util.function.Function;

import com.leroy.ronan.cache.Cache;
import com.leroy.ronan.cache.CacheBuilder;

public class FilePersistedCacheBuilder<K, T>  extends CacheBuilder<K, T>{

	private Function<K, File> keyToFile;
	private Function<T, byte[]> toBytes;
	private Function<byte[], T> fromBytes;

	@Override
	protected Cache<K, T> build(Function<K, T> provider, Duration delay, Duration timeout) {
		return new FilePersistedCache<>(provider, delay, timeout, keyToFile, toBytes, fromBytes);
	}
	
	public FilePersistedCacheBuilder<K, T> storingKey(Function<K, File> keyToFile) {
		this.keyToFile = keyToFile;
		return this;
	}

	public FilePersistedCacheBuilder<K, T> convertingData(Function<T, byte[]> toBytes) {
		this.toBytes = toBytes;
		return this;
	}

	public FilePersistedCacheBuilder<K, T> recoveringData(Function<byte[], T> fromBytes) {
		this.fromBytes = fromBytes;
		return this;
	}
}
