package com.leroy.ronan.cache.persisted;

import java.io.File;
import java.time.Duration;
import java.util.function.Function;

import com.leroy.ronan.cache.Cache;
import com.leroy.ronan.cache.CacheEntry;

public class FilePersistedCache<K, T> extends Cache<K, T> {

	private Function<K, File> keyToFile;
	private Function<T, byte[]> toBytes;
	private Function<byte[], T> fromBytes;
	
	protected FilePersistedCache(Function<K, T> provider, Duration delay, Duration timeout, Function<K, File> keyToFile, Function<T, byte[]> toBytes, Function<byte[], T> fromBytes) {
		super(provider, delay, timeout);
		this.keyToFile = keyToFile;
		this.toBytes = toBytes;
		this.fromBytes = fromBytes;
	}
	
	protected FilePersistedCache(Function<K, T> provider, Duration delay, Function<K, File> keyToFile, Function<T, byte[]> toBytes, Function<byte[], T> fromBytes) {
		this(provider, delay, null, keyToFile, toBytes, fromBytes);
	}

	protected FilePersistedCache(Function<K, T> provider, Function<K, File> keyToFile, Function<T, byte[]> toBytes, Function<byte[], T> fromBytes) {
		this(provider, null, keyToFile, toBytes, fromBytes);
	}

	@Override
	protected CacheEntry<T> retreive(K key) {
		return new FilePersistedCacheEntry<>(keyToFile.apply(key), toBytes, fromBytes);
	}
}
