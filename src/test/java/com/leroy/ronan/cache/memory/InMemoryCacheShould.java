package com.leroy.ronan.cache.memory;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.time.Duration;
import java.util.stream.IntStream;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.leroy.ronan.cache.Cache;
import com.leroy.ronan.cache.beans.StringDataProvider;
import com.leroy.ronan.cache.beans.impl.IncrementalDataProvider;

public class InMemoryCacheShould {

	private String[] keys = {"foo", "bar"};
	private String[] data = {"baz", "qux"};
	private StringDataProvider provider;
	
	@Before
	public void before() {
		provider = Mockito.mock(StringDataProvider.class);
		IntStream.range(0, keys.length)
			.forEach(i -> Mockito.when(provider.apply(keys[i])).thenReturn(data[i]));
	}
	
	@Test
	public void retreive_requested_element() {
		Cache<String, String> cache = new InMemoryCache<>(provider);
		
		String actual = cache.get(keys[0]);

		Assert.assertThat(actual, Matchers.is(data[0]));
	}

	@Test
	public void call_provider_only_once_when_object_is_requested_twice() {
		Cache<String, String> cache = new InMemoryCache<>(provider);
		
		String actual1 = cache.get(keys[0]);
		String actual2 = cache.get(keys[0]);

		assertThat(actual1, is(data[0]));
		assertThat(actual2, is(data[0]));
		verify(provider, times(1)).apply(keys[0]);
	}

	@Test
	public void call_provider_only_once_for_each_object_requested() {
		Cache<String, String> cache = new InMemoryCache<>(provider);
		
		assertThat(data[0], is(not(data[1])));
		assertThat(data[0], is(cache.get(keys[0])));
		assertThat(data[0], is(cache.get(keys[0])));
		assertThat(data[1], is(cache.get(keys[1])));
		assertThat(data[1], is(cache.get(keys[1])));
		verify(provider, times(1)).apply(keys[0]);
		verify(provider, times(1)).apply(keys[1]);
	}
	
	@Test
	public void recall_provider_once_data_is_expired() throws InterruptedException {
		Cache<String, String> cache = new InMemoryCache<>(provider, Duration.ofMillis(10l));

		assertThat(data[0], is(cache.get(keys[0])));
		
		Thread.sleep(Duration.ofMillis(20l).toMillis());
		
		assertThat(data[0], is(cache.get(keys[0])));
		verify(provider, times(2)).apply(keys[0]);
	}
	
	@Test
	public void return_new_data_after_expiration_delay_in_memory() throws InterruptedException {
		Cache<String, Integer> cache = new InMemoryCacheBuilder<String, Integer>()
				.from(new IncrementalDataProvider())
				.expiringAfter(Duration.ofMillis(10l))
				.build();

		assertThat(cache.get(keys[0]), is(1));
		assertThat(cache.get(keys[0]), is(1));
		
		Thread.sleep(Duration.ofMillis(20l).toMillis());

		assertThat(cache.get(keys[0]), is(2));
		assertThat(cache.get(keys[0]), is(2));
	}


}
