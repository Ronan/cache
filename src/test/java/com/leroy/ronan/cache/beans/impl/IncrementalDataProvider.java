package com.leroy.ronan.cache.beans.impl;

import java.util.HashMap;
import java.util.Map;

import com.leroy.ronan.cache.beans.DataProvider;

public class IncrementalDataProvider implements DataProvider<Integer> {

	private Map<String, Integer> content;
	
	public IncrementalDataProvider() {
		content = new HashMap<>();
	}
	
	@Override
	public Integer apply(String key) {
		int res = 0;
		if (content.containsKey(key)) {
			res = content.get(key);
		}
		res++;
		content.put(key, res);
		return res;
	}
}
