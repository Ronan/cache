package com.leroy.ronan.cache.beans.impl;

import java.time.Duration;

import com.leroy.ronan.cache.beans.DataProvider;

public class SlowProvider<T> implements DataProvider<T>{

	private DataProvider<T> provider;
	private Duration duration;
	
	public SlowProvider(DataProvider<T> provider, Duration duration) {
		this.provider = provider;
		this.duration = duration;
	}

	@Override
	public T apply(String key) {
		T res = this.provider.apply(key);
		try {
			Thread.sleep(duration.toMillis());
		} catch (InterruptedException e) {
		}
		return res;
	}
}
