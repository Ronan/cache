package com.leroy.ronan.cache.beans;

import java.util.function.Function;

public interface DataProvider<T> extends Function<String, T> {

}
