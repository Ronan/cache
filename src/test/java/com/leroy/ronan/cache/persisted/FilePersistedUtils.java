package com.leroy.ronan.cache.persisted;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;

public class FilePersistedUtils {

    private static final Logger log = Logger.getLogger(new Object() { }.getClass().getEnclosingClass());

	public static byte[] toBytes(String s) {
		byte[] bytes = null;
		try {
			bytes = s.getBytes("utf-8");
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), e);
		}
		return bytes;
	}
	
	public static byte[] intToBytes(int i) {
		return FilePersistedUtils.toBytes(String.valueOf(i));
	};

	public static String fromBytes(byte[] bytes) {
		String res = null;
		try {
			res = new String(bytes, "utf-8");
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return res;
	};

	public static Integer intFromBytes(byte[] bytes) {
		return Integer.parseInt(FilePersistedUtils.fromBytes(bytes));
	};
}
