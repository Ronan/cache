package com.leroy.ronan.cache.persisted;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.time.Duration;
import java.util.function.Function;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.leroy.ronan.cache.Cache;
import com.leroy.ronan.cache.beans.StringDataProvider;
import com.leroy.ronan.cache.beans.impl.IncrementalDataProvider;

public class FilePersistedCacheShould {

    private String key = "key";
    private String object = "object";
	private StringDataProvider provider;

	private Function<String, File> toFile;
    
    @Before
    public void before() throws IOException {
		provider = Mockito.mock(StringDataProvider.class);
		Mockito.when(provider.apply(key)).thenReturn(object);

		File tmpDir = Files.createTempDirectory("tests").toFile();
		
		toFile = k -> {
			return new File(tmpDir, k+".tmp");
		};

    }
    
	@Test
	public void call_provider_only_once_when_cache_is_relaunched() throws IOException {
		Cache<String, String> cache1 = new FilePersistedCache<>(provider, toFile, FilePersistedUtils::toBytes, FilePersistedUtils::fromBytes);
		assertThat(object, is(cache1.get(key)));

		Cache<String, String> cache2 = new FilePersistedCache<>(provider, toFile, FilePersistedUtils::toBytes, FilePersistedUtils::fromBytes);
		assertThat(object, is(cache2.get(key)));

		verify(provider, times(1)).apply(key);
	}
	
	@Test
	public void recall_provider_once_data_is_expired() throws InterruptedException {
		Cache<String, String> cache = new FilePersistedCache<>(provider, Duration.ofMillis(10l), toFile, FilePersistedUtils::toBytes, FilePersistedUtils::fromBytes);

		assertThat(object, is(cache.get(key)));
		
		Thread.sleep(Duration.ofMillis(20l).toMillis());
		
		assertThat(object, is(cache.get(key)));
		verify(provider, times(2)).apply(key);
	}
	
	@Test
	public void return_new_data_after_expiration_delay_in_file() throws InterruptedException {
		Cache<String, Integer> cache = new FilePersistedCacheBuilder<String, Integer>()
				.storingKey(toFile)
				.convertingData(FilePersistedUtils::intToBytes)
				.recoveringData(FilePersistedUtils::intFromBytes)
				.from(new IncrementalDataProvider())
				.expiringAfter(Duration.ofSeconds(2l))
				.build();

		assertThat(cache.get(key), is(1));
		assertThat(cache.get(key), is(1));
		
		Thread.sleep(Duration.ofSeconds(4l).toMillis());

		assertThat(cache.get(key), is(2));
		assertThat(cache.get(key), is(2));
	}
}
