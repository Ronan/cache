package com.leroy.ronan.cache;

import static java.lang.Thread.sleep;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import java.time.Duration;

import org.junit.Test;

import com.leroy.ronan.cache.beans.impl.IncrementalDataProvider;
import com.leroy.ronan.cache.beans.impl.SlowProvider;
import com.leroy.ronan.cache.memory.InMemoryCacheBuilder;

public class CacheShould {

    private String key = "key";
    
    private static final long TICK = 500l;
    private static final Duration PROVIDER_DELAY   = Duration.ofMillis( 4 * TICK);
    private static final Duration EXPIRATION_DELAY = Duration.ofMillis(40 * TICK);
    private static final Duration TIMEOUT_DELAY    = Duration.ofMillis( 2 * TICK);
    
    @Test(timeout= 3 * TICK)
    public void return_no_data_if_provider_is_too_slow() throws InterruptedException {
        Cache<String, Integer> cache = new InMemoryCacheBuilder<String, Integer>()
                .from(new SlowProvider<>(new IncrementalDataProvider(), PROVIDER_DELAY))
                .expiringAfter(EXPIRATION_DELAY)
                .timingOutAfter(TIMEOUT_DELAY)
                .build();

        assertThat(cache.get(key), nullValue());
    }
    
    @Test(timeout= 8 * TICK)
    public void return_data_after_wait_if_provider_is_too_slow() throws InterruptedException {
        Cache<String, Integer> cache = new InMemoryCacheBuilder<String, Integer>()
                .from(new SlowProvider<>(new IncrementalDataProvider(), PROVIDER_DELAY))
                .expiringAfter(EXPIRATION_DELAY)
                .timingOutAfter(TIMEOUT_DELAY)
                .build();

        assertThat(cache.get(key), nullValue());
        sleep(TIMEOUT_DELAY.toMillis());
        assertThat(cache.get(key), is(1));
    }
    
}
